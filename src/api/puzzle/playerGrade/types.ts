export interface PlayerGradeVO {
  /**
   * ID
   */
  id: string | number;

  /**
   * 用户ID
   */
  userId: string | number;

  /**
   * 完成时间 秒
   */
  finishTime: number;

  /**
   * 用户名
   */
  username: string;

  /**
   * 用户昵称
   */
  nickname: string;

  /**
   * 周
   */
  week: string;

  /**
   * 月
   */
  month: string;

  /**
   * 年
   */
  year: string;

  /**
   * 游戏难度 简单、中等、困难
   */
  gameLevel: string;

  /**
   * 备注
   */
  remark: string;

}

export interface PlayerGradeForm extends BaseEntity {
  /**
   * ID
   */
  id?: string | number;

  /**
   * 用户ID
   */
  userId?: string | number;

  /**
   * 完成时间 秒
   */
  finishTime?: number;

  /**
   * 用户名
   */
  username?: string;

  /**
   * 用户昵称
   */
  nickname?: string;

  /**
   * 周
   */
  week?: string;

  /**
   * 月
   */
  month?: string;

  /**
   * 年
   */
  year?: string;

  /**
   * 游戏难度 简单、中等、困难
   */
  gameLevel?: string;

  /**
   * 备注
   */
  remark?: string;

}

export interface PlayerGradeQuery extends PageQuery {

  /**
   * 用户ID
   */
  userId?: string | number;

  /**
   * 完成时间 秒
   */
  finishTime?: number;

  /**
   * 用户名
   */
  username?: string;

  /**
   * 用户昵称
   */
  nickname?: string;

  /**
   * 周
   */
  week?: string;

  /**
   * 月
   */
  month?: string;

  /**
   * 年
   */
  year?: string;

  /**
   * 游戏难度 简单、中等、困难
   */
  gameLevel?: string;

    /**
     * 日期范围参数
     */
    params?: any;
}



