import request from '@/utils/request';
import { AxiosPromise } from 'axios';
import { PlayerGradeVO, PlayerGradeForm, PlayerGradeQuery } from '@/api/puzzle/playerGrade/types';

/**
 * 查询玩家游戏记录列表
 * @param query
 * @returns {*}
 */

export const listPlayerGrade = (query?: PlayerGradeQuery): AxiosPromise<PlayerGradeVO[]> => {
  return request({
    url: '/puzzle/playerGrade/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询玩家游戏记录详细
 * @param id
 */
export const getPlayerGrade = (id: string | number): AxiosPromise<PlayerGradeVO> => {
  return request({
    url: '/puzzle/playerGrade/' + id,
    method: 'get'
  });
};

/**
 * 新增玩家游戏记录
 * @param data
 */
export const addPlayerGrade = (data: PlayerGradeForm) => {
  return request({
    url: '/puzzle/playerGrade',
    method: 'post',
    data: data
  });
};

/**
 * 修改玩家游戏记录
 * @param data
 */
export const updatePlayerGrade = (data: PlayerGradeForm) => {
  return request({
    url: '/puzzle/playerGrade',
    method: 'put',
    data: data
  });
};

/**
 * 删除玩家游戏记录
 * @param id
 */
export const delPlayerGrade = (id: string | number | Array<string | number>) => {
  return request({
    url: '/puzzle/playerGrade/' + id,
    method: 'delete'
  });
};
