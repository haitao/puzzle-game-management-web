import request from '@/utils/request';
import { AxiosPromise } from 'axios';
import { PlayerVO, PlayerForm, PlayerQuery } from '@/api/puzzle/player/types';

/**
 * 查询玩家信息列表
 * @param query
 * @returns {*}
 */

export const listPlayer = (query?: PlayerQuery): AxiosPromise<PlayerVO[]> => {
  return request({
    url: '/puzzle/player/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询玩家信息详细
 * @param id
 */
export const getPlayer = (id: string | number): AxiosPromise<PlayerVO> => {
  return request({
    url: '/puzzle/player/' + id,
    method: 'get'
  });
};

/**
 * 新增玩家信息
 * @param data
 */
export const addPlayer = (data: PlayerForm) => {
  return request({
    url: '/puzzle/player',
    method: 'post',
    data: data
  });
};

/**
 * 修改玩家信息
 * @param data
 */
export const updatePlayer = (data: PlayerForm) => {
  return request({
    url: '/puzzle/player',
    method: 'put',
    data: data
  });
};

/**
 * 删除玩家信息
 * @param id
 */
export const delPlayer = (id: string | number | Array<string | number>) => {
  return request({
    url: '/puzzle/player/' + id,
    method: 'delete'
  });
};
