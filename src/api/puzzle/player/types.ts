export interface PlayerVO {
  /**
   * 用户ID
   */
  id: string | number;

  /**
   * 用户账号
   */
  userName: string;

  /**
   * 用户昵称
   */
  nickName: string;

  /**
   * 用户邮箱
   */
  email: string;

  /**
   * 手机号码
   */
  phonenumber: string;

  /**
   * 密码
   */
  password: string;

  /**
   * 帐号状态（0正常 1停用）
   */
  status: string;

  /**
   * 备注
   */
  remark: string;

}

export interface PlayerForm extends BaseEntity {
  /**
   * 用户ID
   */
  id?: string | number;

  /**
   * 用户账号
   */
  userName?: string;

  /**
   * 用户昵称
   */
  nickName?: string;

  /**
   * 用户邮箱
   */
  email?: string;

  /**
   * 手机号码
   */
  phonenumber?: string;

  /**
   * 密码
   */
  password?: string;

  /**
   * 帐号状态（0正常 1停用）
   */
  status?: string;

  /**
   * 备注
   */
  remark?: string;

}

export interface PlayerQuery extends PageQuery {

  /**
   * 用户账号
   */
  userName?: string;

  /**
   * 用户昵称
   */
  nickName?: string;

  /**
   * 用户邮箱
   */
  email?: string;

  /**
   * 手机号码
   */
  phonenumber?: string;

  /**
   * 密码
   */
  password?: string;

  /**
   * 帐号状态（0正常 1停用）
   */
  status?: string;

    /**
     * 日期范围参数
     */
    params?: any;
}



