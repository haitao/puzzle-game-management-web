import request from '@/utils/request';
import { AxiosPromise } from 'axios';
import { StoryVO, StoryForm, StoryQuery } from '@/api/puzzle/story/types';

/**
 * 查询故事典故列表
 * @param query
 * @returns {*}
 */

export const listStory = (query?: StoryQuery): AxiosPromise<StoryVO[]> => {
  return request({
    url: '/puzzle/story/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询故事典故详细
 * @param id
 */
export const getStory = (id: string | number): AxiosPromise<StoryVO> => {
  return request({
    url: '/puzzle/story/' + id,
    method: 'get'
  });
};

/**
 * 新增故事典故
 * @param data
 */
export const addStory = (data: StoryForm) => {
  return request({
    url: '/puzzle/story',
    method: 'post',
    data: data
  });
};

/**
 * 修改故事典故
 * @param data
 */
export const updateStory = (data: StoryForm) => {
  return request({
    url: '/puzzle/story',
    method: 'put',
    data: data
  });
};

/**
 * 删除故事典故
 * @param id
 */
export const delStory = (id: string | number | Array<string | number>) => {
  return request({
    url: '/puzzle/story/' + id,
    method: 'delete'
  });
};
