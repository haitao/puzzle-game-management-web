export interface StoryVO {
  /**
   * 用户ID
   */
  id: string | number;

  /**
   * 标题
   */
  title: string;

  /**
   * 作者
   */
  author: string;

  /**
   * 内容
   */
  content: string;

  /**
   * 状态（0正常 1停用）
   */
  status: string;

  /**
   * 备注
   */
  remark: string;

}

export interface StoryForm extends BaseEntity {
  /**
   * 用户ID
   */
  id?: string | number;

  /**
   * 标题
   */
  title?: string;

  /**
   * 作者
   */
  author?: string;

  /**
   * 内容
   */
  content?: string;

  /**
   * 状态（0正常 1停用）
   */
  status?: string;

  /**
   * 备注
   */
  remark?: string;

}

export interface StoryQuery extends PageQuery {

  /**
   * 标题
   */
  title?: string;

  /**
   * 作者
   */
  author?: string;

  /**
   * 内容
   */
  content?: string;

  /**
   * 状态（0正常 1停用）
   */
  status?: string;

    /**
     * 日期范围参数
     */
    params?: any;
}



