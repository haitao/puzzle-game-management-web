import request from '@/utils/request';
import { AxiosPromise } from 'axios';
import { PlayerPayRecordVO, PlayerPayRecordForm, PlayerPayRecordQuery } from '@/api/puzzle/playerPayRecord/types';

/**
 * 查询玩家付款记录列表
 * @param query
 * @returns {*}
 */

export const listPlayerPayRecord = (query?: PlayerPayRecordQuery): AxiosPromise<PlayerPayRecordVO[]> => {
  return request({
    url: '/puzzle/playerPayRecord/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询玩家付款记录详细
 * @param id
 */
export const getPlayerPayRecord = (id: string | number): AxiosPromise<PlayerPayRecordVO> => {
  return request({
    url: '/puzzle/playerPayRecord/' + id,
    method: 'get'
  });
};

/**
 * 新增玩家付款记录
 * @param data
 */
export const addPlayerPayRecord = (data: PlayerPayRecordForm) => {
  return request({
    url: '/puzzle/playerPayRecord',
    method: 'post',
    data: data
  });
};

/**
 * 修改玩家付款记录
 * @param data
 */
export const updatePlayerPayRecord = (data: PlayerPayRecordForm) => {
  return request({
    url: '/puzzle/playerPayRecord',
    method: 'put',
    data: data
  });
};

/**
 * 删除玩家付款记录
 * @param id
 */
export const delPlayerPayRecord = (id: string | number | Array<string | number>) => {
  return request({
    url: '/puzzle/playerPayRecord/' + id,
    method: 'delete'
  });
};
