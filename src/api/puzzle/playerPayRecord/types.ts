export interface PlayerPayRecordVO {
  /**
   * ID
   */
  id: string | number;

  /**
   * 用户ID
   */
  userId: string | number;

  /**
   * 交易ID
   */
  transactionId: string | number;

  /**
   * 金额
   */
  amount: string;

  /**
   * 微信ID
   */
  wxId: string | number;

  /**
   * 支付状态（0正常 1异常）
   */
  status: string;

  /**
   * 备注
   */
  remark: string;

}

export interface PlayerPayRecordForm extends BaseEntity {
  /**
   * ID
   */
  id?: string | number;

  /**
   * 用户ID
   */
  userId?: string | number;

  /**
   * 交易ID
   */
  transactionId?: string | number;

  /**
   * 金额
   */
  amount?: string;

  /**
   * 微信ID
   */
  wxId?: string | number;

  /**
   * 支付状态（0正常 1异常）
   */
  status?: string;

  /**
   * 备注
   */
  remark?: string;

}

export interface PlayerPayRecordQuery extends PageQuery {

  /**
   * 用户ID
   */
  userId?: string | number;

  /**
   * 交易ID
   */
  transactionId?: string | number;

  /**
   * 金额
   */
  amount?: string;

  /**
   * 微信ID
   */
  wxId?: string | number;

  /**
   * 支付状态（0正常 1异常）
   */
  status?: string;

    /**
     * 日期范围参数
     */
    params?: any;
}



